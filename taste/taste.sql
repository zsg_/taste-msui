/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.6.11 : Database - taste
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`taste` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `taste`;

/*Table structure for table `t_master` */

DROP TABLE IF EXISTS `t_master`;

CREATE TABLE `t_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_path` varchar(128) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `from_addr` varchar(64) DEFAULT NULL,
  `type_name` varchar(64) DEFAULT NULL,
  `summary` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `t_master` */

insert  into `t_master`(`id`,`img_path`,`title`,`from_addr`,`type_name`,`summary`) values (1,'/images/master/master_1.png','浇汁豆腐','出品/北京 小老虎糖水铺','地方风味','无论从口感还是味道，都极接近地恢复了南方黑芝麻糊。'),(2,'/images/master/master_2.png','海南飘香味 豉油皇文昌鸡','出品/北京 小老虎糖水铺','地方风味','无论从口感还是味道，都极接近地恢复了南方黑芝麻糊。'),(3,'/images/master/master_3.png','挑战重口味 水煮肉片','出品/北京 小老虎糖水铺','地方风味','无论从口感还是味道，都极接近地恢复了南方黑芝麻糊。'),(4,'/images/master/master_4.png','周末聚会小吃 鮟鱇鱼火腿','出品/北京 小老虎糖水铺','地方风味','无论从口感还是味道，都极接近地恢复了南方黑芝麻糊。'),(5,'/images/master/master_5.png','一看就会的广式花生薄','出品/北京 小老虎糖水铺','地方风味','无论从口感还是味道，都极接近地恢复了南方黑芝麻糊。'),(6,'/images/master/master_6.png','没人不爱吃的什锦饭','出品/北京 小老虎糖水铺','地方风味','无论从口感还是味道，都极接近地恢复了南方黑芝麻糊。'),(7,'/images/master/master_7.png','凉拌小鲜 毛蛤蜊拌菠菜','出品/北京 小老虎糖水铺','地方风味','无论从口感还是味道，都极接近地恢复了南方黑芝麻糊。'),(8,'/images/master/master_8.png','盛夏必吃！紫花菌煨青椒','出品/北京 小老虎糖水铺','地方风味','无论从口感还是味道，都极接近地恢复了南方黑芝麻糊。');

/*Table structure for table `t_master_material` */

DROP TABLE IF EXISTS `t_master_material`;

CREATE TABLE `t_master_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) DEFAULT NULL,
  `material_name` varchar(64) DEFAULT NULL,
  `material_weight` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `t_master_material` */

insert  into `t_master_material`(`id`,`master_id`,`material_name`,`material_weight`) values (1,7,'黑芝麻','100g'),(2,7,'花生','50g'),(3,7,'大米','50g'),(4,7,'糖粉','50g'),(5,7,'糯米粉','120g'),(11,8,'黑芝麻','100g'),(12,8,'花生','50g'),(13,8,'大米','50g'),(14,8,'糖粉','50g'),(15,8,'糯米粉','120g');

/*Table structure for table `t_master_method` */

DROP TABLE IF EXISTS `t_master_method`;

CREATE TABLE `t_master_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) DEFAULT NULL,
  `step_num` int(11) DEFAULT NULL,
  `step_content` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `t_master_method` */

insert  into `t_master_method`(`id`,`master_id`,`step_num`,`step_content`) values (1,8,1,'黑芝麻、花生、大米洗干净后用小火炒熟。因为量少，所以用锅炒反而比烤箱快。同时糯米粉放进烤箱，150℃烤15分钟至颜色微微发黄。'),(2,8,2,'大米炒到色泽金黄。黑芝麻炒到开始发出“卜卜卜”的小声响时就关火，不然焦了就会有苦味。花生炒熟后剥皮。'),(3,8,3,'大米先磨粉。花生和芝麻在打粉的时候会出油，所以研磨到没有大颗粒时，就混入糖粉、大米粉、糯米粉继续研磨，这样就能得到细腻的粉末。'),(4,8,4,'这是研磨好的。如果你在研磨时没有混合均匀，就把最后得到的所有粉末装进保鲜袋，打结，摇均匀。然后装进罐子里密封保存。'),(5,8,5,'每天早晨，舀四汤勺（就吃饭喝汤的小汤勺）芝麻粉于杯子里，开水冲泡，配上一块三明治，营养又健康~'),(6,7,1,'黑芝麻、花生、大米洗干净后用小火炒熟。因为量少，所以用锅炒反而比烤箱快。同时糯米粉放进烤箱，150℃烤15分钟至颜色微微发黄。'),(7,7,2,'大米炒到色泽金黄。黑芝麻炒到开始发出“卜卜卜”的小声响时就关火，不然焦了就会有苦味。花生炒熟后剥皮。'),(8,7,3,'大米先磨粉。花生和芝麻在打粉的时候会出油，所以研磨到没有大颗粒时，就混入糖粉、大米粉、糯米粉继续研磨，这样就能得到细腻的粉末。'),(9,7,4,'这是研磨好的。如果你在研磨时没有混合均匀，就把最后得到的所有粉末装进保鲜袋，打结，摇均匀。然后装进罐子里密封保存。'),(10,7,5,'每天早晨，舀四汤勺（就吃饭喝汤的小汤勺）芝麻粉于杯子里，开水冲泡，配上一块三明治，营养又健康~');

/*Table structure for table `t_master_tip` */

DROP TABLE IF EXISTS `t_master_tip`;

CREATE TABLE `t_master_tip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) DEFAULT NULL,
  `content` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `t_master_tip` */

insert  into `t_master_tip`(`id`,`master_id`,`content`) values (1,8,'制作较多的芝麻粉时，可以把花生放进烤箱，170℃烤15分钟。芝麻还是建议用锅老老实实地炒制，不然很容易就过头焦了。'),(2,8,'有的人嫌芝麻的量太少了，你可以自己酌量增加，但不宜太多。因为黑芝麻润肠，吃少量通便，多了可能就会拉稀了。据说一天不要吃超过一两。'),(3,7,'制作较多的芝麻粉时，可以把花生放进烤箱，170℃烤15分钟。芝麻还是建议用锅老老实实地炒制，不然很容易就过头焦了。'),(4,7,'有的人嫌芝麻的量太少了，你可以自己酌量增加，但不宜太多。因为黑芝麻润肠，吃少量通便，多了可能就会拉稀了。据说一天不要吃超过一两。');

/*Table structure for table `t_news` */

DROP TABLE IF EXISTS `t_news`;

CREATE TABLE `t_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_path` varchar(128) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `descp` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `t_news` */

insert  into `t_news`(`id`,`img_path`,`title`,`descp`) values (1,'/images/news/news_1.png','香葱烟肉小饼','烟肉即培根，烟肉和小葱作馅料，淡淡的烟肉香味和浓郁的葱香相互交融，面饼入口松软酥香，不油不腻，和面的时候加了糖，咸口回甜。'),(2,'/images/news/news_2.png','味觉操控','美食，顾名思义就是美味的食物，贵的有山珍海味，便宜的有街边小吃。其实美食是不分贵贱的，只要是自己喜欢的，都可以称之为美食。美食遭遇心情的时候，美食已不仅仅是简单的味觉感受，更是一种精神享受。也是一种场合。场合好吃起来也有味道、人人都想吃上自己喜欢的。世界各地美食文化博大精深，营养物质各不相同，品味更多美食，享受更多健康。1.jpg 不同国家的历史有长有短，疆域有大有小，实力有强有弱，人口有多有少，民族构成、宗教信仰、政权性质和经济结构也有差异，故而各国的饮食文化是不一样的。中国的烹饪，不仅技术精湛，而且有讲究菜肴美感的传统，注意食物的色、香、味、形、器的协调一致。对菜肴美感的表现是多方面的，无论是个红萝卜，还是一个白菜心，都可以雕出各种造型，独树一帜，达到色、香、味、形、美的和谐统一，给人以精神和物质高度统一的特殊享受。'),(3,'/images/news/news_3.png','香酥黄金锤','这个估计都是小朋友的大爱，我不喜欢买这一些给他们吃。可是小朋友有的时候也会爱吃，总是拗不过小朋友的哀求。刚好家里有面包糠就做一点给他们解馋，自己做的比较放心，也省钱。炸好的时候我都快没忍住下口了，这个“黄金锤”配上亨氏番茄酱简直就是绝配。色香味俱全，还好小朋友不在家不然连拍照的机会都木有。放学接小朋友回家小朋友看到桌上的“黄金锤”高兴的大叫，我的最爱还没有洗手都直接抓了吃揽都来不及，吃了一口才想起还没有洗手。没过几分钟就被他们“光盘行动了”还跟我约时间还要再做给他们尝尝。纠结这个毕竟是油炸的上火，我先答应了。我要求他们要喝绿豆汤降火下次再做。');

/*Table structure for table `t_restaurant` */

DROP TABLE IF EXISTS `t_restaurant`;

CREATE TABLE `t_restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_path` varchar(128) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `subtitle` varchar(128) DEFAULT NULL,
  `addr` varchar(50) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `t_restaurant` */

insert  into `t_restaurant`(`id`,`img_path`,`title`,`subtitle`,`addr`,`comment`) values (1,'/images/restaurant/index_restaurant_1.png','福楼法餐厅','法国餐饮界的领军者','北京','北京福楼法餐厅装修极其讲究，带有浓重的新派艺术风格，犹如身在巴黎的“Terminus Nord\"。，室内铺着木质地板，墙壁上满满地画着十九世纪二十年代初巴黎的景致。绘画表现了巴黎刚通火车时的社会文化背景及巴黎上层人士以乘火车为时尚的场景。旁边体态幽雅的铜铸古典美女雕像，让本已充满怀旧氛围的餐厅更加温馨雅致。 整个餐厅并不太大，排列紧凑的桌椅以及简洁明快的桌饰，让人远离高不可攀的浮华，有一种老派贵族风范。法国著名雕塑家E.Carlier的黄铜作品，BelleEpoqueEra的墙面绘画，以及新艺术派手工制作的彩色玻璃，这一切的一切都为北京福楼法餐厅增添了更多的文化色彩。'),(2,'/images/restaurant/index_restaurant_2.png','Peter Luger','世界十大牛排馆之一、地道纽约客必到之处','上海','也许中国客人比较熟悉巴菲特最钟爱的牛排馆smith & wollensky，但它并非纽约人心目中的NO.1。在纽约，有一家百年传奇牛排馆Peter Luger连续30年被Zagat评为纽约最棒牛排馆，美国唯一一家米其林星级牛排馆。它几乎成为了“牛排”的代名词，布鲁克林的地标。即使曼哈顿有无数个高档牛排餐厅，但红肉爱好者还是会驱车前往布鲁克林桥边的这家百年老店，它的牛排成为众多餐厅争相模仿的典范。如果没有品尝过一次Peter Luger，总觉得没有完全体味纽约，它的味道让人无法忘怀。'),(3,'/images/restaurant/index_restaurant_3.png','Finca Altonano','法国餐饮界的领军者','北京','北京福楼法餐厅装修极其讲究，带有浓重的新派艺术风格，犹如身在巴黎的“Terminus Nord\"。，室内铺着木质地板，墙壁上满满地画着十九世纪二十年代初巴黎的景致。绘画表现了巴黎刚通火车时的社会文化背景及巴黎上层人士以乘火车为时尚的场景。旁边体态幽雅的铜铸古典美女雕像，让本已充满怀旧氛围的餐厅更加温馨雅致。 整个餐厅并不太大，排列紧凑的桌椅以及简洁明快的桌饰，让人远离高不可攀的浮华，有一种老派贵族风范。法国著名雕塑家E.Carlier的黄铜作品，BelleEpoqueEra的墙面绘画，以及新艺术派手工制作的彩色玻璃，这一切的一切都为北京福楼法餐厅增添了更多的文化色彩。');

/*Table structure for table `t_show` */

DROP TABLE IF EXISTS `t_show`;

CREATE TABLE `t_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `img_path` varchar(128) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `descp` varchar(256) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `star_num` int(11) DEFAULT NULL,
  `comment_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `t_show` */

insert  into `t_show`(`id`,`user_id`,`img_path`,`title`,`descp`,`createtime`,`star_num`,`comment_num`) values (1,1,'/images/show/show_1.png','浇汁豆腐','铁板豆腐是老豆腐在铁板上煎熟之后，撒上酱料吃的。我回来如法炮制了，用平底不粘锅代替铁板，虽然是南豆腐，但口感一样好，只是煎的时候比较容易碎。煎之前我还多做了一道功序，豆腐用盐水煮过，不仅去除了豆腥味，而且豆腐完全入味了。这道豆腐可以当点心吃，作为菜肴待客也是很不错的！','2016-07-27 19:20:08',15,1),(2,2,'/images/show/show_2.png','清蒸糯米糕','亲戚给了一些自己磨的大米粉，一直不知道怎么吃。偶尔看到了蒸米糕的方法，试着一做，真是太好吃了。满口大米的清香，软糯又有嚼劲。蒸了好多，分给邻居，获得了一致好评。而且最最关键的是超级简单啊，完全零难度。','2016-07-27 20:17:21',14,1),(3,3,'/images/show/show_3.png','鸡蛋肠粉','“肠粉粉是网购的，网购一直没做啊，下次试试看自己配粉，那就想吃就能自己做来吃，其实做肠粉也不难啊，就是麻烦点啊。配料按自己喜欢添加。','2016-07-26 20:17:24',12,1);

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(48) DEFAULT NULL,
  `showname` varchar(48) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`username`,`showname`,`password`) values (1,'test','林霞','1'),(2,'test2','萌宠','1'),(3,'test3','奥特曼','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
