package pluto.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseShow<M extends BaseShow<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setUserId(java.lang.Integer userId) {
		set("user_id", userId);
	}

	public java.lang.Integer getUserId() {
		return get("user_id");
	}

	public void setImgPath(java.lang.String imgPath) {
		set("img_path", imgPath);
	}

	public java.lang.String getImgPath() {
		return get("img_path");
	}

	public void setTitle(java.lang.String title) {
		set("title", title);
	}

	public java.lang.String getTitle() {
		return get("title");
	}

	public void setDescp(java.lang.String descp) {
		set("descp", descp);
	}

	public java.lang.String getDescp() {
		return get("descp");
	}

	public void setCreatetime(java.util.Date createtime) {
		set("createtime", createtime);
	}

	public java.util.Date getCreatetime() {
		return get("createtime");
	}

	public void setStarNum(java.lang.Integer starNum) {
		set("star_num", starNum);
	}

	public java.lang.Integer getStarNum() {
		return get("star_num");
	}

	public void setCommentNum(java.lang.Integer commentNum) {
		set("comment_num", commentNum);
	}

	public java.lang.Integer getCommentNum() {
		return get("comment_num");
	}

}
