package pluto.controller;

import pluto.controller.base.BaseController;
import pluto.controller.dto.MyAppJSON;
import pluto.model.News;
import pluto.model.Show;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.Restful;
public class NewsController extends BaseController {
	public void index(){
		MyAppJSON<News> appJSON=new MyAppJSON();
		appJSON.addItems(News.dao.findForIndex(10).getList());
		renderJson(appJSON);
	}
	public void show(){
//		setAttr("one", News.dao.findById(getParaToInt()));
//		myRender("/news/detail.html");
		MyAppJSON<News> appJSON=new MyAppJSON<News>();
		appJSON.addItem(News.dao.findById(getParaToInt()));
		renderJson(appJSON);
	}
}
