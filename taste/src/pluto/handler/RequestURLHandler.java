package pluto.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

public class RequestURLHandler extends Handler{
	private String baseURL="BASE_URL";

	@Override
	public void handle(String target, HttpServletRequest request,
			HttpServletResponse response, boolean[] isHandled) {
		request.setAttribute(baseURL, request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
		next.handle(target, request, response, isHandled);
	}

}
